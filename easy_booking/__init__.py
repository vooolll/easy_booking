from functools import wraps
import os
from flask import Flask, redirect
from flask.helpers import url_for
from flask_login import LoginManager, current_user
from easy_booking.models.user import User

app = Flask(__name__)
app.secret_key = "didar raq"

app.config['DEFAULT_FILE_STORAGE'] = 'filesystem'
app.config['UPLOADS_FOLDER'] = os.path.realpath('.') + '/easy_booking/static/uploads/'
app.config['FILE_SYSTEM_STORAGE_FILE_VIEW'] = 'static'

login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@login_manager.unauthorized_handler
def unauthorized():
    return redirect('login')

from controllers import users, index, fields