import unittest
from pymongo import MongoClient
from easy_booking.models.notififcation import Notification
from easy_booking.models.user import User, user_col


class UserTest(unittest.TestCase):

    def setUp(self):
        self.col = MongoClient()['dev']['user']
        self.user = User({"phone": "+7701 384 07 70", "password": "secret", "role": 3})
        self.user.save()

    def tearDown(self):
        user_col.drop()

    def test_init(self):
        self.assertIsNotNone(self.user)
        self.assertIsNotNone(self.user.dic)

    def test_save(self):
        saved_user = self.col.find_one({"phone": "+7701 384 07 70"})
        self.assertIsNotNone(saved_user)

    def test_phone_exists(self):
        self.assertTrue(self.user.phone_exists("701 384 07 70"))
        self.assertFalse(self.user.phone_exists("-123"))

    def test_pass_change(self):
        prev_pass = self.user.dic['password']
        new_pass = "not secret"
        self.assertNotEquals(prev_pass, new_pass)
        self.user.pass_change(new_pass)
        self.assertNotEquals(self.col.find_one({"phone": "+7701 384 07 70"})['password'], prev_pass)
        self.assertEquals(self.col.find_one({"phone": "+7701 384 07 70"})['password'], new_pass)

    def test_pass_match(self):
        self.assertTrue(self.user.pass_match("701 384 07 70", "secret"))

    def test_get(self):
        id = self.user.dic['_id']
        self.assertIsNotNone(User.get(id))
        self.assertEquals(User.get(id).dic['phone'], self.user.dic['phone'])

    def test_get_by_phone(self):
        self.assertEquals(self.user.dic['phone'], User.get_by_phone("701 384 07 70").dic['phone'])
        self.assertEquals(self.user.dic['password'], User.get_by_phone("701 384 07 70").dic['password'])

    def test_admin_ids(self):
        second_admin = User({"phone": "+7701 384 07 71", "password": "secret", "role": 3})
        second_admin.save()
        print "DEBUG: " + str(User.admin_ids())
        self.assertEquals(self.col.find_one({"role": 3})['_id'], User.admin_ids()[0])
        self.assertEquals(self.col.find_one({"role": 3, "phone": "+7701 384 07 71"})["_id"], User.admin_ids()[1])

    def test_is_applied(self):
        second_user = User({"phone": "+7701 384 07 71", "password": "secret", "role": 3})
        second_user.save()
        self.notif = Notification({"user_id": self.user.dic['_id'], "type": 0, "admin_ids": []})
        self.notif.send_to_admin()
        self.assertTrue(self.user.is_applied())
        self.assertFalse(second_user.is_applied())
        MongoClient()['dev']['notification'].drop()