import unittest
from easy_booking.models.field import Field, field_col
from easy_booking.models.notififcation import notif_col


class FieldTest(unittest.TestCase):

    def setUp(self):
        self.field = Field({"facilities": [], "user_id": "1123"})

    def tearDown(self):
        notif_col.drop()

    def test_init(self):
        self.assertIsNotNone(self.field)

    def test_save(self):
        self.field.save()
        self.assertIsNotNone(self.field.id)

    def test_all(self):
        self.assertEquals(len(Field.all()), len(list(field_col.find())))
#TODO finish tests

