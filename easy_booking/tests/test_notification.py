import unittest

from easy_booking import User
from easy_booking.models.field import Field, field_col
from easy_booking.models.notififcation import Notification, notif_col
from easy_booking.models.user import user_col


class NotificationTest(unittest.TestCase):
    def setUp(self):
        self.user = User({"phone": "+7701 384 07 70", "password": "secret", "role": 3})
        self.user.save()
        self.field = Field({"user_id": self.user.id, "name": "FootballLife"})
        self.field.save()
        self.notif = Notification({"user_id": self.user.id, "type": 0, "admin_ids": []})
        self.notif1 = Notification({"field_id": self.field.id, "type": 1})

    def tearDown(self):
        user_col.drop()
        notif_col.drop()
        field_col.drop()

    def test__init(self):
        print self.notif.dic
        self.assertEquals(self.notif.dic, {"user_id": self.user.id, "type": 0, "admin_ids": []})

    def test_send_to_admin(self):
        self.notif.send_to_admin()
        self.assertEquals(notif_col.find_one({"admin_ids": User.admin_ids()})['type'], 0)
        self.assertEquals(notif_col.find_one({"admin_ids": User.admin_ids()})['user_id'], self.user.id)

    def test_all(self):
        self.assertEquals(len(Notification.all()), len(list(notif_col.find())))

    def test_send_to_admin1(self):
        self.notif1.send_to_admin()
        self.assertEquals(notif_col.find_one({"type": 1})["field_id"], self.field.id)
        self.assertEquals(notif_col.find_one({"field_id": self.field.id})["type"], 1)
        self.assertEquals(notif_col.find_one({"type": 1}), self.notif1.dic)