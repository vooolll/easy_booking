# coding=utf-8
from bson import ObjectId
from easy_booking.db import db
from easy_booking.models.notififcation import notif_col

field_col = db['field']


class Field():

    def __init__(self, dic):
        self.dic = dic
        self.user_id = dic['user_id']

    def save(self):
        self.id = field_col.insert(self.dic)

    def send_admin(self):
        notif_col.save({"type": 1, "_id": self.id})

    @staticmethod
    def update_with(id, dic):
        field_col.update({"_id": ObjectId(id)}, dic)

    @staticmethod
    def all():
        return field_col.find()

    @staticmethod
    def load(id):
        return field_col.find_one({"_id": ObjectId(id)})

    @staticmethod
    def fields_for(id):
        return field_col.find({"user_id": id})

    @staticmethod
    def all():
        return list(field_col.find())