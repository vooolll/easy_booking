from bson.objectid import ObjectId
from easy_booking.db import db


user_col = db["user"]


class User():

    def __init__(self, dic):
        self.dic = dic

    # Should be implemented for flask-login
    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def is_active(self):
        return True

    def get_id(self):
        return unicode(str(self.id))

    def save(self):
        self.id = user_col.insert(self.dic)

    def phone_exists(self, phone):
        if user_col.find_one({'phone': "+7" + phone}) is not None:
            return True
        else:
            return False

    def pass_change(self, password):
        user_col.update({'phone': self.dic['phone']}, {'$set': {'password': password}})

    def pass_match(self, phone, password):
        if user_col.find_one({'phone': "+7" + phone})['password'] == password:
            return True
        else:
            return False

    def update(self):
        user_col.update({"_id": self.dic['_id']}, self.dic)

    def is_applied(self):
        col1 = db['notification']
        if col1.find_one({"user_id": self.dic['_id']}) is not None:
            return True
        else:
            return False

    @staticmethod
    def get(user_id):
        return User(user_col.find_one({'_id': ObjectId(user_id)}))

    @staticmethod
    def get_by_phone(phone):
        dic = user_col.find_one({'phone': "+7" + phone})
        print phone
        user = User(dic)
        if dic is None:
            return None
        else:
            user.id = dic['_id']
            return user

    @staticmethod
    def admin_ids():
        user_ids = []
        for user in user_col.find():
            if user['role'] is 3:
                user_ids.append(user['_id'])
        return user_ids

    @staticmethod
    def all():
        return user_col.find()