from easy_booking import User
from easy_booking.db import db

notif_col = db["notification"]


class Notification():

    def __init__(self, dic):
        self.dic = dic

    def send_to_admin(self):
        self.dic["admin_ids"] = User.admin_ids()
        self.id = notif_col.save(self.dic)

    def remove(self):
        notif_col.remove()

    @staticmethod
    def for_user(id):
        return Notification(notif_col.find_one({"user_id": id}))

    @staticmethod
    def providers():
        provs = []
        for notification in Notification.all():
            if notification['type'] is 0:
                provs.append(User.get(unicode(notification['user_id'])))
                #print User.get(notification['user_id']).dic
        return provs

    @staticmethod
    def all():
        return list(notif_col.find())

    @staticmethod
    def fields():
        pass