# -*- coding: utf-8 -*-


from flask import render_template, url_for, redirect, request
from flask_login import login_user, login_required, logout_user, current_user
from easy_booking.models.field import Field
from easy_booking.models.notififcation import Notification
from forms import Registration, Login, ChangePass
from easy_booking.models.user import User
from easy_booking import app
from helpers import admin_only


@app.route("/home/")
@login_required
def home():
    fields = Field.fields_for(current_user.dic['_id'])
    return render_template("home.html", fields=fields)


@app.route("/process_reg", methods=['POST'])
def process_reg():
    # Роли: 1 - юзер, 2 - владелец поля, 3 - администратор
    filled_form = Registration(request.form)
    if filled_form.validate():
        doc = {"name": filled_form.username.data, "phone": "+7" + filled_form.phone.data,
               "password": filled_form.password.data,
               "role": 1, "fields": []}
        user = User(doc)
        print filled_form.phone.data
        if not user.phone_exists(filled_form.phone.data):
            user.save()
            return redirect(url_for("login"))
        else:
            return render_template("registration.html", form=filled_form,
                                   error=u"Пользователь с таким номером уже сущесвует")
    else:
        return render_template("registration.html", form=filled_form)


@app.route("/reg")
def reg():
    empty_form = Registration()
    return render_template("registration.html", form=empty_form)


@app.route("/login")
def login():
    empty_form = Login()
    return render_template("login.html", form=empty_form)


@app.route("/process_log", methods=['POST'])
def process_login():
    filled_form = Login(request.form)
    if filled_form.validate():
        phone = filled_form.phone.data
        password = filled_form.password.data
        user = User.get_by_phone(phone)
        print user
        if user is None:
            return render_template("login.html", form=filled_form, error=u"Не верный логин или пароль")
        else:
            print user.phone_exists(phone)
            if user.phone_exists(phone) and user.pass_match(phone, password):
                login_user(user)
                return redirect(url_for("index"))
            else:
                return render_template("login.html", form=filled_form, error=u"Не верный логин или пароль")
    else:
        return render_template("login.html", form=filled_form)


@app.route("/change_pass")
@login_required
def change_pass():
    empty_form = ChangePass()
    return render_template('change_pass.html', form=empty_form)


@app.route("/change", methods=['POST'])
@login_required
def change():
    filled_form = ChangePass(request.form)
    if filled_form.validate():
        old_p = filled_form.old_password.data
        new_p = filled_form.new_password.data
        if current_user.dic['password'] == old_p:
            current_user.pass_change(new_p)
            return redirect(url_for('home'))
        else:
            return render_template('change_pass.html', form=filled_form, error=u"Не верный старый пароль")
    else:
        return render_template('change_pass.html', form=filled_form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))


@app.route("/prov_notify")
@login_required
def provider_notify():
    if current_user.is_applied():
        return render_template("home.html", message=u"Ваш запрос рассматривается")
    else:
        notification = Notification({"user_id": current_user.dic['_id'], "type": 0})
        notification.send_to_admin()
        return render_template("home.html", message=u"Запрос будет рассмотрен ближайшее время")


@app.route("/admin_notifications")
@login_required
@admin_only
def admin_notifs():
    return render_template("notifications.html", providers=Notification.providers())


@app.route("/not_allowed")
def not_allowed():
    return render_template("not_allowed.html")


@app.route("/user_info/<id>")
@login_required
@admin_only
def user_info(id):
    return render_template("user_info.html", user=User.get(id))


@app.route("/accept/<id>")
@login_required
@admin_only
def accept_provider(id):
    user = User.get(id)
    user.dic['role'] = 2
    user.update()
    this_notif = Notification.for_user(user.dic['_id'])
    this_notif.remove()
    return redirect(url_for("admin_notifs"))

@app.route("/decline/<id>")
@admin_only
@login_required
def decline_provider(id):
    user = User.get(id)
    this_notif = Notification.for_user(user.dic['_id'])
    this_notif.remove()
    return redirect(url_for("admin_notifs"))


@app.route("/all_users")
@login_required
@admin_only
def all_users():
    return render_template("all_users.html", users=User.all())