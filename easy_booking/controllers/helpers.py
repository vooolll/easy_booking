from functools import wraps
from flask import url_for
from flask.ext.login import current_user
from werkzeug.utils import redirect


def admin_only(controller):
    @wraps(controller)
    def wrapped(*args, **kwargs):
        if current_user.dic['role'] is 3:
            return controller(*args, **kwargs)
        else:
            return redirect(url_for("not_allowed"))
    return wrapped


def provider_only(controller):
    @wraps(controller)
    def wrapped(*args, **kwargs):
        if current_user.dic['role'] is 2:
            return controller(*args, **kwargs)
        else:
            return redirect(url_for("not_allowed"))
    return wrapped


def provider_admin_only(controller):
    @wraps(controller)
    def wrapped(*args, **kwargs):
        if current_user.dic['role'] is 2 or 3:
            return controller(*args, **kwargs)
        else:
            return redirect(url_for("not_allowed"))
    return wrapped