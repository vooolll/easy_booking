# -*- coding: utf-8 -*-
from wtforms import Form, TextField, PasswordField, validators, TextAreaField, SelectMultipleField, widgets


def is_phone():
    pass


class Registration(Form):
    username = TextField(u'Имя пользователя',
                         [validators.Length(min=4, max=25, message=u"Имя должно быть длинной от 4 до 25 символов")])
    phone = TextField(u'Номер телефона',
                      [validators.Length(min=10, max=12, message=u"Введите одиннадцатизначный номер")])
    password = PasswordField(u'Пароль', [
        validators.Length(min=6, message=U"Слишком короткий пароль, минимум 6 символов"),
        validators.Required(message=u"Поле обязательное"),
        validators.EqualTo('confirm', message=u'Пароли должны совпадать')
    ])
    confirm = PasswordField(u'Повторите пароль')


class Login(Form):
    phone = TextField(u"Номер телефона", [validators.Required(message=u"Поле обязательное")])
    password = PasswordField(u"Пароль", [
        validators.Required(message=u"Поле обязательное")])


class ChangePass(Form):
    old_password = PasswordField(u'Старый пароль', [
        validators.Required(message=u"Поле обязательное"),
        validators.Length(min=6, message=u"Слишком короткий пароль")
    ])

    new_password = PasswordField(u'Новый пароль', [
        validators.Length(min=6, message=u"Слишком короткий пароль"),
        validators.Required(message=u"Поле обязательное"),
        validators.EqualTo('confirm', message=u'Пароли должны совпадать')
    ])

    confirm = PasswordField(u'Повторите пароль')


class MultiCheckboxField(SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce sub fields, allowing custom rendering of
    the enclosed checkbox fields.
    """
    widget = widgets.ListWidget(prefix_label=True)
    option_widget = widgets.CheckboxInput()

class FieldFootball(Form):
    org_name = TextField(u'Название организации', [validators.Required()])
    address = TextAreaField(u'Полный адрес футбольного поля',[validators.Required()])
    price = TextField(u'Цена', [validators.Required(), validators.NumberRange()])
    phone = TextField(u'Телефоны', [validators.Required()])
    phone1 = TextField(u'')
    phone2 = TextField(u'')
    my_choices = [(u'Трава', u'Трава'), (u'Крытый', u'Крытый'), (u'Трибуны', u'Трибуны')]
    facilities = MultiCheckboxField(choices = my_choices)