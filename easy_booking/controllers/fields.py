import os

from werkzeug.utils import secure_filename
from flask import render_template, url_for, redirect, request
from flask.ext.login import login_required
from flask_login import current_user
from easy_booking import app
from easy_booking.models.notififcation import Notification
from helpers import provider_admin_only, admin_only

from easy_booking.models.field import Field
from forms import FieldFootball


@app.route("/add_field")
@login_required
@provider_admin_only
def add_field():
    #TODO Security check is provider
    empty_form = FieldFootball()
    return render_template("add_field.html", form=empty_form)


@app.route("/process_field", methods=['POST'])
@login_required
@provider_admin_only
def process_field():
    #TODO Security check is provider
    filled_form = FieldFootball(request.form)
    if filled_form.validate():
        uploaded_files = request.files.getlist("file[]")
        file_names = []
        for file in uploaded_files:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOADS_FOLDER'], filename))
            file_names.append(filename)
        doc = {"organization": filled_form.org_name.data, "address": filled_form.address.data,
               "file_names": file_names, "price": filled_form.price.data, "phone": "+7" + filled_form.phone.data,
               "phone1": "+7" + filled_form.phone1.data, "phone2": "+7" + filled_form.phone2.data,
               "user_id": current_user.dic['_id'], "facilities": filled_form.facilities.data}
        field = Field(doc)
        field.save()
        notif = Notification({"field_id": field.id, "type": 1})
        notif.send_to_admin()
        #TODO notify admin: field added
        return redirect(url_for('field_profile', id=field.id))
    else:
        return render_template("add_field.html", form=filled_form)


@app.route("/field/<id>")
@login_required
def field_profile(id):
    field = Field.load(id)
    return render_template("field_home.html", field=field, path="/static/uploads/")


#TODO edit football field
@app.route("/field/edit/<id>")
@login_required
@provider_admin_only
def field_edit(id):
    dic_to_edit = Field.load(id)
    form = FieldFootball(request.form, org_name=dic_to_edit['organization'],
                         address=dic_to_edit['address'], price=dic_to_edit['price'], phone=dic_to_edit['phone'],
                         phone1=dic_to_edit['phone1'], phone2=dic_to_edit['phone2'],
                         facilities=dic_to_edit['facilities'], file=dic_to_edit['file_names'])
    return render_template("edit_field.html", form=form, id=id)


@app.route("/field/process_edit/<id>", methods=["POST"])
@login_required
@provider_admin_only
def process_edit(id):
    filled_form = FieldFootball(request.form)
    if filled_form.validate():
        uploaded_files = request.files.getlist("file[]")
        file_names = []
        for file in uploaded_files:
            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOADS_FOLDER'], filename))
                file_names.append(filename)
        doc = {"organization": filled_form.org_name.data, "address": filled_form.address.data,
               "file_names": file_names, "price": filled_form.price.data, "phone": "+7" + filled_form.phone.data,
               "phone1": "+7" + filled_form.phone1.data, "phone2": "+7" + filled_form.phone2.data,
               "user_id": current_user.dic['_id'], "facilities": filled_form.facilities.data}
        Field.update_with(id, doc)
        return redirect(url_for('field_profile', id=id))
    else:
        return render_template("add_field.html", form=filled_form)


@app.route("/all_fields")
@login_required
@admin_only
def all_fields():
    return render_template("all_fields.html", fields=Field.all())